import pathlib
import sys
import tarfile
from typing import Dict, List, Tuple, Union


# Flawed type definition
TAR_MEMBER_INDEX = Dict[Union[None, str], Union["TAR_MEMBER_INDEX", tarfile.TarInfo]]


class TarTree(object):
    __slots__ = [
        "_tree",
    ]

    def __init__(self, members: List[tarfile.TarInfo]) -> None:
        self._tree: TAR_MEMBER_INDEX = {}
        for member in members:
            self.add(member)

    def _get_subtree_at_path(self, path: pathlib.PurePosixPath) -> TAR_MEMBER_INDEX:
        if path.is_absolute():
            raise ValueError("The path must be relative to the root")
        current_path = pathlib.PurePosixPath()
        subtree = self._tree
        for part in path.parts:
            current_path = current_path / part
            try:
                subtree = subtree[part]
            except KeyError:
                raise KeyError("{!s:s} does not exist".format(current_path)) from None
        return subtree

    def __getitem__(self, path: Union[str, pathlib.PurePosixPath]) -> tarfile.TarInfo:
        if isinstance(path, str):
            path = pathlib.PurePosixPath(path)
        subtree = self._get_subtree_at_path(path)
        try:
            return subtree[None]
        except KeyError:
            raise KeyError("{!s:s} has no TarInfo entry".format(path)) from None

    def add(self, tarinfo: tarfile.TarInfo) -> None:
        path = pathlib.PurePosixPath(tarinfo.name)
        if path.is_absolute():
            raise ValueError("The path must be relative to the root")
        subtree = self._tree
        for elem in path.parts:
            try:
                subtree = subtree[elem]
            except KeyError:
                new_subtree = {}
                subtree[elem] = new_subtree
                subtree = new_subtree
        subtree[None] = tarinfo

    def remove(self, path: Union[str, pathlib.PurePosixPath]) -> None:
        if isinstance(path, str):
            path = pathlib.PurePosixPath(path)
        if path.is_absolute():
            raise ValueError("The path must be relative to the root")
        subtree = self._get_subtree_at_path(path.parent)
        del subtree[path.name]

    def members(self, path: Union[str, pathlib.PurePosixPath]) -> List[Tuple[str, tarfile.TarInfo]]:
        subtree = self._get_subtree_at_path(path)
        return [(k, v[None]) for k, v in subtree.items() if k is not None and None in v]

    def realpath(self, path: Union[str, pathlib.PurePosixPath]) -> pathlib.PurePosixPath:
        """Returns the realpath
        
        The implementation is flawed because TarTree does not guarantee the tree
        to be a coherent file hierarchy.
        """
        if isinstance(path, str):
            path = pathlib.PurePosixPath(path)
        if not path.is_absolute():
            raise ValueError("The path must be absolute")
        parts_to_evaluate = list(path.relative_to("/").parts)
        evaluated_path = pathlib.PurePosixPath("/")
        while len(parts_to_evaluate) > 0:
            part = parts_to_evaluate[0]
            del parts_to_evaluate[0]
            if part == ".":
                continue
            elif part == "..":
                if evaluated_path == "/":
                    raise ValueError("Encountered too many ..")
                evaluated_path = evaluated_path.parent
                continue
            member = self[evaluated_path / part]
            if member.islnk() or member.issym():
                target = pathlib.PurePosixPath(member.linkname)
                if target.is_absolute():
                    evaluated_path = pathlib.PurePosixPath("/")
                    parts_to_evaluate = list(target.relative_to("/").parts)
                else:
                    parts_to_evaluate[0:0] = list(target.parts)
            else:
                evaluated_path = evaluated_path / part
        return evaluated_path
