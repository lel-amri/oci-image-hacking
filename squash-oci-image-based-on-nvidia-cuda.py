import difflib
import enum
import json
import os.path
import tarfile
import pathlib
import re
import sys
from typing import Any, Dict, List, Optional, Union

from tar_tree import TarTree
import zran_file_object
from oci_spec import Algorithm, Digest


class OCIMediaType(object):
    def __init__(self, s: str) -> None:
        parts = s.split("+", 1)
        if len(parts) == 1:
            left, right = parts[0], None
        elif len(parts) == 2:
            left, right = parts
        else:
            assert False
        kind, format, compression = None, None, None
        if left == "application/vnd.oci.descriptor.v1" and right == "json":
            kind = left
            format = right
            compression = None
        elif left == "application/vnd.oci.layout.header.v1" and right == "json":
            kind = left
            format = right
            compression = None
        elif left == "application/vnd.oci.image.index.v1" and right == "json":
            kind = left
            format = right
            compression = None
        elif left == "application/vnd.oci.image.manifest.v1" and right == "json":
            kind = left
            format = right
            compression = None
        elif left == "application/vnd.oci.image.config.v1" and right == "json":
            kind = left
            format = right
            compression = None
        elif left == "application/vnd.oci.image.layer.v1.tar":
            kind = left
            if right == "gzip":
                compression = right
            elif right == "zstd":
                compression = right
            if kind is None or compression is None:
                kind = None
                compression = None
        if kind is None:
            raise ValueError("Unsupported media type: {:s}".format(s))
        assert kind is not None and ((format is not None and compression is None) or (format is None and compression is not None))
        self.kind, self.format, self.compression = kind, format, compression

    def __str__(self) -> str:
        if self.format is None and self.compression is None:
            return self.kind
        else:
            return "{:s}+{:s}".format(self.kind, self.format or self.compression)

    def __repr__(self) -> str:
        return f"OCIMediaType({self!r:s})"


def layer_detect_ubuntu_start(tar: tarfile.TarFile, tar_tree: TarTree) -> bool:
    path = pathlib.PurePosixPath("/etc/os-release")
    try:
        member_os_release = tar_tree[path]
    except KeyError:
        return False
    if member_os_release.islnk() or member_os_release.issym():
        member_os_release = tar_tree[tar_tree.realpath(path)]
    if not member_os_release.isreg():
        return False
    file_os_release = tar.extractfile(member_os_release)
    content_os_release = file_os_release.read(4 * 1024**1)
    for line in content_os_release.split(b"\n"):
        line = line.strip()
        if line == b"ID=ubuntu":
            break
    else:
        return False
    return True


def layer_detect_nvidia_cuda_base_start(tar: tarfile.TarFile, tar_tree: TarTree) -> bool:
    path = pathlib.PurePosixPath("/etc/apt/sources.list.d/cuda-ubuntu2204-x86_64.list")
    try:
        member_sources_list = tar_tree[path]
    except KeyError:
        return False
    return True


def layer_detect_nvidia_cuda_base_end(tar: tarfile.TarFile, tar_tree: TarTree) -> bool:
    path = pathlib.PurePosixPath("/NGC-DL-CONTAINER-LICENSE")
    try:
        member_license = tar_tree[path]
    except KeyError:
        return False
    return True


def layer_detect_nvidia_cuda_runtime_start(tar: tarfile.TarFile, tar_tree: TarTree) -> bool:
    path = pathlib.PurePosixPath("/var/lib/dpkg/info")
    try:
        members = tar_tree.members(path)
    except KeyError:
        return False
    for name, _ in members:
        if re.match(r"^cuda-libraries-[0-9.-]+\.list$", name):
            break
    else:
        return False
    return True


def layer_detect_nvidia_cuda_runtime_end(tar: tarfile.TarFile, tar_tree: TarTree) -> bool:
    path = pathlib.PurePosixPath("/opt/nvidia/nvidia_entrypoint.sh")
    try:
        member_license = tar_tree[path]
    except KeyError:
        return False
    return True


def layer_detect_nvidia_cuda_devel_start(tar: tarfile.TarFile, tar_tree: TarTree) -> bool:
    path = pathlib.PurePosixPath("/var/lib/dpkg/info")
    try:
        members = tar_tree.members(path)
    except KeyError:
        return False
    for name, _ in members:
        if re.match(r"^cuda-libraries-dev-[0-9.-]+\.list$", name):
            break
    else:
        return False
    return True


def layer_detect_nvidia_cuda_devel_end(tar: tarfile.TarFile, tar_tree: TarTree) -> bool:
    path_dpkg_status = pathlib.PurePosixPath("/var/lib/dpkg/status")
    path_dpkg_status_old = pathlib.PurePosixPath("/var/lib/dpkg/status-old")
    try:
        member_dpkg_status = tar_tree[path_dpkg_status]
        member_dpkg_status_old = tar_tree[path_dpkg_status_old]
    except KeyError:
        return False
    if not member_dpkg_status.isreg() or not member_dpkg_status_old.isreg():
        return False
    file_dpkg_status = tar.extractfile(member_dpkg_status)
    file_dpkg_status_old = tar.extractfile(member_dpkg_status_old)
    content_dpkg_status = file_dpkg_status.read(4 * 1024**2).decode(encoding="utf-8").split("\n")
    content_dpkg_status_old = file_dpkg_status_old.read(4 * 1024**2).decode(encoding="utf-8").split("\n")
    diff = list(difflib.unified_diff(content_dpkg_status_old, content_dpkg_status))
    for line in diff:
        if "Package: libnccl-dev" in line:
            break
    else:
        return False
    for line in diff:
        if "Package: libcublas-dev" in line:
            break
    else:
        return False
    return True


def digest_to_path(digest: Digest) -> str:
    return os.path.join(str(digest.algorithm), digest.encoded)


def recursively_get_members(tartree: TarTree, path: Union[str, pathlib.PurePosixPath], ordered: bool = False) -> Any:
    def _recursively_get_members(tartree: TarTree, path: pathlib.PurePosixPath, ordered: bool) -> Any:
        try:
            yield tartree[path]
        except KeyError:
            pass
        for name, _ in sorted(tartree.members(path), key=lambda e: e[0]):
            yield from _recursively_get_members(tartree, path / name, ordered)
    if isinstance(path, str):
        path = pathlib.PurePosixPath(path)
    return _recursively_get_members(tartree, path, ordered)


def main():
    oci_directory = sys.argv[1]
    with open(os.path.join(oci_directory, "index.json"), mode="r") as fh:
        index = json.load(fh)
    manifest_digest = Digest(index["manifests"][0]["digest"])
    with open(os.path.join(oci_directory, "blobs", digest_to_path(manifest_digest)), mode="r") as fh:
        manifest = json.load(fh)
    tarinfo_to_layer = {}
    layers_fh = []
    layers_tar_tree = []
    layer_tarfile = []
    for i, layer in enumerate(manifest["layers"]):
        media_type = OCIMediaType(layer["mediaType"])
        layer_digest = Digest(layer["digest"])
        layer_path = os.path.join(oci_directory, "blobs", digest_to_path(layer_digest))
        if media_type.kind not in ("application/vnd.oci.image.layer.v1.tar", "application/vnd.oci.image.layer.nondistributable.v1.tar"):
            print(f"Unsupported layer kind: {media_type.kind:s}", file=sys.stderr)
            sys.exit(1)
        if media_type.compression is not None:
            if media_type.compression != "gzip":
                print(f"Unsupported layer compression: {media_type.compression:s}", file=sys.stderr)
                sys.exit(1)
            sys.stdout.write("Indexing compressed layer {!s:s} ({:d}/{:d})...".format(layer_digest, i + 1, len(manifest["layers"])))
            sys.stdout.flush()
            fh = zran_file_object.ZranFile(layer_path)
            sys.stdout.write(" DONE\n")
            sys.stdout.flush()
        else:
            fh = open(layer_path, mode="rb", buffering=0)
        layers_fh.append(fh)
        sys.stdout.write("Reading layer {!s:s} ({:d}/{:d})...".format(layer_digest, i + 1, len(manifest["layers"])))
        sys.stdout.flush()
        tar = tarfile.TarFile(sys.argv[1], mode='r', fileobj=fh)
        members = tar.getmembers()
        for member in members:
            tarinfo_to_layer[member] = i
        tar_tree = TarTree(members)
        sys.stdout.write(" DONE\n")
        sys.stdout.flush()
        layers_tar_tree.append(tar_tree)
        layer_tarfile.append(tar)
    squashed_tar_tree = TarTree([])
    for i, tar_tree in enumerate(layers_tar_tree):
        if i < 10:
            continue
        sys.stdout.write("Squashing layer {!s:s} ({:d}/{:d})...".format(layer_digest, i + 1, len(manifest["layers"])))
        sys.stdout.flush()
        for elem in recursively_get_members(tar_tree, ""):
            path = pathlib.PurePosixPath(elem.name)
            if path.name[:4] == ".wh.":
                if path.name == ".wh..wh.opq":
                    target = path.parent
                    for elem in recursively_get_members(tar_tree, target):
                        try:
                            squashed_tar_tree.remove(elem.name)
                        except KeyError:
                            squashed_tar_tree.add(elem)
                else:
                    target = path.parent / path.name[4:]
                    try:
                        squashed_tar_tree.remove(target)
                    except KeyError:
                        squashed_tar_tree.add(elem)
        for elem in recursively_get_members(tar_tree, ""):
            path = pathlib.PurePosixPath(elem.name)
            if path.name[:4] != ".wh.":
                squashed_tar_tree.add(elem)
        sys.stdout.write(" DONE\n")
        sys.stdout.flush()
    with open(os.path.join(oci_directory, "blobs", "sha256", "squashed_layer"), mode="wb") as fh:
        squashed_tarfile = tarfile.TarFile(mode="w", fileobj=fh)
        sys.stdout.write("Writing squashed layer...")
        sys.stdout.flush()
        for elem in recursively_get_members(squashed_tar_tree, "", ordered=True):
            layer_i = tarinfo_to_layer[elem]
            if elem.isreg():
                fileobj = layer_tarfile[layer_i].extractfile(elem)
            else:
                fileobj = None
            squashed_tarfile.addfile(elem, fileobj=fileobj)
        sys.stdout.write(" DONE\n")
        sys.stdout.flush()


if False:
    if layer_detect_ubuntu_start(tar, tar_tree):
        print("Ubuntu start")
    if layer_detect_nvidia_cuda_base_start(tar, tar_tree):
        print("NVIDIA CUDA base start")
    if layer_detect_nvidia_cuda_base_end(tar, tar_tree):
        print("NVIDIA CUDA base end")
    if layer_detect_nvidia_cuda_runtime_start(tar, tar_tree):
        print("NVIDIA CUDA runtime start")
    if layer_detect_nvidia_cuda_runtime_end(tar, tar_tree):
        print("NVIDIA CUDA runtime end")
    if layer_detect_nvidia_cuda_devel_start(tar, tar_tree):
        print("NVIDIA CUDA devel start")
    if layer_detect_nvidia_cuda_devel_end(tar, tar_tree):
        print("NVIDIA CUDA devel end")

if __name__ == "__main__":
    main()
