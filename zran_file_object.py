import os

import zran


class ZranFile(object):
    __slots__ = [
        "_fh",
        "_index",
        "_zran_index",
        "_pos",
    ]

    def __init__(self, file):
        self._fh = open(file, mode="rb", buffering=0)
        self._index = zran.Index.create_index(self._fh)
        self._zran_index = self._index.to_c_index()
        self._pos = self._fh.seek(0, os.SEEK_CUR)
        self.seek(0, os.SEEK_SET)

    def tell(self):
        return self.seek(0, os.SEEK_CUR)

    def seek(self, offset, whence=os.SEEK_SET):
        if whence == os.SEEK_SET:
            if offset < 0:
                raise OSError("seeked before the start")
            if offset > self._index.uncompressed_size:
                raise OSError("seeked after the end")
            self._pos = offset
        elif whence == os.SEEK_END:
            if offset < -self._uncompressed_size:
                raise OSError("seeked before the start")
            if offset > 0:
                raise OSError("seeked after the end")
            self._pos = self._uncompressed_size - offset
        elif whence == os.SEEK_CUR:
            candidate_pos = self._pos + offset
            if offset < 0:
                raise OSError("seeked before the start")
            if offset > self._index.uncompressed_size:
                raise OSError("seeked after the end")
            self._pos = candidate_pos
        else:
            raise ValueError("Unsupported whence")
        return self._pos

    def read(self, n):
        output = zran.decompress(self._fh, self._zran_index, self._pos, n)
        self._pos = min(self._index.uncompressed_size, self._pos + len(output))
        return output

    def __enter__(self):
        if self._fh.closed:
            raise ValueError("I/O operation on closed file.")

    def __exit__(self, exc_type, exc_value, traceback):
        self._fh.close()
