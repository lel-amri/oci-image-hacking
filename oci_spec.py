import hashlib
import re
from collections import OrderedDict
from typing import Any, BinaryIO, Dict, List, Type, Union


_re_algorithm = re.compile(r"[a-z0-9]+(?:[.+_-][a-z0-9]+)*")
_re_encoded = re.compile(r"[a-zA-Z0-9=_-]+")
_re_digest = re.compile(fr"^(?P<algorithm>{_re_algorithm.pattern:s}):(?P<encoded>{_re_encoded.pattern:s})$")


class AlgorithmMeta(type):
    tree = OrderedDict()
    veins = {}
    string_matches = {}

    def __new__(cls, name: str, bases: List[Type], namespace: Dict[str, Any], **kwargs):
        def get_parent(bases):
            parent = None
            for base in bases:
                if type(base) == AlgorithmMeta:
                    parent = base
            return parent
        def register(obj):
            nonlocal cls
            parent = get_parent(bases)
            if parent is None:
                vein = []
            else:
                vein = [parent] + cls.veins[parent]
            subtree = cls.tree
            for e in reversed(vein):
                subtree = subtree[e]
            subtree[obj] = OrderedDict()
            cls.veins[obj] = vein
        def walk_tree(tree):
            nonlocal cls
            ret = []
            for obj in tree.keys():
                ret.append(obj)
            for subtree in tree.values():
                ret.extend(walk_tree(subtree))
            return ret
        def generate_new_new(original_new):
            def replacement_new(cls_, algorithm, *args, **kwargs):
                nonlocal original_new
                nonlocal walk_tree
                nonlocal cls
                vein = cls.veins[cls_]
                if len(vein) > 0:
                    super_ = super(cls_, vein[-1])
                else:
                    super_ = super(cls_, cls_)
                objs = [cls_] + walk_tree(cls.tree[cls_])
                can_match = False
                for obj in objs:
                    if obj in cls.string_matches:
                        can_match = True
                        if cls.string_matches[obj] == algorithm:
                            if original_new is not None:
                                return original_new(obj, *args, **kwargs)
                            else:
                                return super_.__new__(obj, *args, **kwargs)
                if can_match:
                    raise ValueError(f"Unsupported algorithm {algorithm!r:s}")
                else:
                    raise RuntimeError(f"{cls_.__name__!s:s} cannot be instanciated")
            return replacement_new
        match_string = namespace.pop("MATCH_STRING", None)
        original_new = namespace.pop("__new__", None)
        namespace["__new__"] = generate_new_new(original_new)
        obj = super().__new__(cls, name, bases, namespace, **kwargs)
        if match_string is not None:
            cls.string_matches[obj] = match_string
        register(obj)
        return obj


class Algorithm(object, metaclass=AlgorithmMeta):
    __slots__ = [
        "_algorithm",
    ]

    def __init__(self, algorithm: str) -> None:
        self._algorithm = algorithm

    @property
    def hash(self):
        raise NotImplementedError()

    @property
    def encode(self, digest: bytes) -> str:
        raise NotImplementedError()

    @property
    def size(self) -> int:
        return self.hash().digest_size

    def __str__(self) -> str:
        return self._algorithm

    def __repr__(self) -> str:
        return f"{type(self).__name__!s:s}({str(self)!r:s})"

    def digest(self, o: Union[bytes, BinaryIO]) -> "Digest":
        h = self.hash()
        if isinstance(o, bytes):
            h.update(o)
        else:
            buffer = bytearray(1 * 1024**2)
            while True:
                n = o.readinto(buffer)
                if n == 0:
                    break
                h.update(buffer[:n])
        d = h.digest()
        return Digest(f"{self!s:s}:{self.encode(d)}")


class _AlgorithmMixin_HexEncoded(object):
    def encode(self, digest: bytes) -> str:
        return digest.hex()


class AlgorithmSha256(_AlgorithmMixin_HexEncoded, Algorithm):
    MATCH_STRING = "sha256"

    def __init__(self, algorithm: str) -> None:
        if algorithm != "sha256":
            raise ValueError(f"Unsupported algorithm {algorithm!r:s}")
        super().__init__(algorithm)

    def hash(self):
        return hashlib.sha256()


class AlgorithmSha512(_AlgorithmMixin_HexEncoded, Algorithm):
    MATCH_STRING = "sha512"

    def __init__(self, algorithm: str) -> None:
        if algorithm != "sha512":
            raise ValueError(f"Unsupported algorithm {algorithm!r:s}")
        super().__init__(algorithm)

    def hash(self):
        return hashlib.sha512()


class Digest(object):
    __slots__ = [
        "_algorithm",
        "_encoded",
    ]

    def __init__(self, digest: str) -> None:
        match = _re_digest.match(digest)
        if match is None:
            raise ValueError(f"Malformed digest {digest!r}")
        self._algorithm = match.group("algorithm")
        self._encoded = match.group("encoded")

    @property
    def algorithm(self) -> "Algorithm":
        return Algorithm(self._algorithm)

    @property
    def encoded(self) -> str:
        return self._encoded

    def __str__(self) -> str:
        return f"{self._algorithm:s}:{self._encoded:s}"

    def __repr__(self) -> str:
        return f"Digest({str(self)!r:s})"
